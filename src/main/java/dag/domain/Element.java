package dag.domain;

import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Element {

	private Collection<Element> parents = new HashSet<>();
	private Collection<Element> children = new HashSet<>();
	private String name;

	public Element(String name) {
		this.name = name;
	}

	public Collection<Element> getChidren() {
		return children;
	}

	public Element addChild(Element child) {
		children.add(child);
		return this;
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Element other = (Element) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public Collection<Element> getParents() {
		return parents;
	}

	public Element addParent(Element parent) {
		parents.add(parent);
		return this;
	}

	public Collection<Element> getDescendants() {
		return Stream.concat(children.stream(), children.stream().flatMap(e -> e.getDescendants().stream())).collect(Collectors.toSet());
	}
	
	public Collection<Element> getAscendants() {
		return Stream.concat(parents.stream(), parents.stream().flatMap(e -> e.getAscendants().stream())).collect(Collectors.toSet());
	}
	
	@Override
	public String toString() {
		return name;
	}

}
