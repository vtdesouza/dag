package dag.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dag.domain.Element;

class ElementTest {

	private Element football;
	private Element competition;
	private Element player;
	private Element premiership;
	private Element championsLeague;
	private Element manCity;
	private Element manUnited;

	@BeforeEach
	void setUp() throws Exception {
		football = new Element("Football");
		competition = new Element("Competition");
		player = new Element("Player");
		premiership = new Element("Premiership");;
		championsLeague = new Element("ChampionsLeague");
		manCity = new Element("ManCity");
		manUnited = new Element("ManUnited");
	}
	
	@Test
	void testAddChildren() {

		assertTrue(football.getChidren().isEmpty());

		football.addChild(competition);

		assertEquals(1, football.getChidren().size());
		assertTrue(football.getChidren().contains(competition));

		football.addChild(competition);

		assertEquals(1, football.getChidren().size());
		assertTrue(football.getChidren().contains(competition));
	}

	@Test
	void testAddParent() {
		
		assertTrue(competition.getParents().isEmpty());

		competition.addParent(football);

		assertEquals(1, competition.getParents().size());
		assertTrue(competition.getParents().contains(football));

		competition.addParent(football);

		assertEquals(1, competition.getParents().size());
		assertTrue(competition.getParents().contains(football));

	}
	
	@Test
	void testDescendants() {
		football.addChild(player);
		football.addChild(competition);
		competition.addChild(premiership);
		competition.addChild(championsLeague);
		premiership.addChild(manCity);
		premiership.addChild(manUnited);
		championsLeague.addChild(manUnited);
		
		Set<Element> decendants = new HashSet<>(Arrays.asList(player, competition, premiership, championsLeague, manCity, manUnited));
		
		assertEquals(decendants, football.getDescendants());
	}
	
	@Test
	void testAscendants() {
		manCity.addParent(premiership);
		manUnited.addParent(premiership);
		manUnited.addParent(championsLeague);
		championsLeague.addParent(competition);
		premiership.addParent(competition);
		competition.addParent(football);
		player.addParent(football);
		
		Set<Element> ascendantsPlayer = new HashSet<>(Arrays.asList(football));
		Set<Element> ascendantsManCity = new HashSet<>(Arrays.asList(football, premiership, competition));
		Set<Element> ascendantsManUnited = new HashSet<>(Arrays.asList(football, premiership, competition, championsLeague));
				
		assertEquals(ascendantsPlayer, player.getAscendants());
		assertEquals(ascendantsManCity, manCity.getAscendants());
		assertEquals(ascendantsManUnited, manUnited.getAscendants());
	}

}
